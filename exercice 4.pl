% importer 'exercice 3.pl' avant

concat3([A,B,C],[D,E,F],[G,H,I],X) :-
	X = [A,B,C,D,E,F,G,H,I].

lignes_correctes([ [ [[A1,B1,C1],[D1,E1,F1],[G1,H1,I1]],
				[[A2,B2,C2],[D2,E2,F2],[G2,H2,I2]],
				[[A3,B3,C3],[D3,E3,F3],[G3,H3,I3]] ],
				[ [[A4,B4,C4],[D4,E4,F4],[G4,H4,I4]],
				[[A5,B5,C5],[D5,E5,F5],[G5,H5,I5]],
				[[A6,B6,C6],[D6,E6,F6],[G6,H6,I6]] ],
				[ [[A7,B7,C7],[D7,E7,F7],[G7,H7,I7]],
				[[A8,B8,C8],[D8,E8,F8],[G8,H8,I8]],
				[[A9,B9,C9],[D9,E9,F9],[G9,H9,I9]] ] ]) :-
	carre([ [A1,B1,C1],[A2,B2,C2],[A3,B3,C3] ]),  % ligne 1 en partant du haut
	carre([ [D1,E1,F1],[D2,E2,F2],[D3,E3,F3] ]),  % ligne 2
	carre([ [G1,H1,I1],[G2,H2,I2],[G3,H3,I3] ]),  % ligne 3
	carre([ [A4,B4,C4],[A5,B5,C5],[A6,B6,C6] ]),  % etc
	carre([ [D4,E4,F4],[D5,E5,F5],[D6,E6,F6] ]),
	carre([ [G4,H4,I4],[G5,H5,I5],[G6,H6,I6] ]),
	carre([ [A7,B7,C7],[A8,B8,C8],[A9,B9,C9] ]),
	carre([ [D7,E7,F7],[D8,E8,F8],[D9,E9,F9] ]),
	carre([ [G7,H7,I7],[G8,H8,I8],[G9,H9,I9] ]).

colonnes_correctes([ [ [[A1,B1,C1],[D1,E1,F1],[G1,H1,I1]],
				[[A2,B2,C2],[D2,E2,F2],[G2,H2,I2]],
				[[A3,B3,C3],[D3,E3,F3],[G3,H3,I3]] ],
				[ [[A4,B4,C4],[D4,E4,F4],[G4,H4,I4]],
				[[A5,B5,C5],[D5,E5,F5],[G5,H5,I5]],
				[[A6,B6,C6],[D6,E6,F6],[G6,H6,I6]] ],
				[ [[A7,B7,C7],[D7,E7,F7],[G7,H7,I7]],
				[[A8,B8,C8],[D8,E8,F8],[G8,H8,I8]],
				[[A9,B9,C9],[D9,E9,F9],[G9,H9,I9]] ] ]) :-
	carre([ [A1,D1,G1],[A4,D4,G4],[A7,D7,G7] ]),  % colonne 1 en partant de la gauche
	carre([ [B1,E1,H1],[B4,E4,H4],[B7,E7,H7] ]),  % colonne 2
	carre([ [C1,F1,I1],[C4,F4,I4],[C7,F7,I7] ]),  % colonne 3
	carre([ [A2,D2,G2],[A5,D5,G5],[A8,D8,G8] ]),  % etc
	carre([ [B2,E2,H2],[B5,E5,H5],[B8,E8,H8] ]),
	carre([ [C2,F2,I2],[C5,F5,I5],[C8,F8,I8] ]),
	carre([ [A3,D3,G3],[A6,D6,G6],[A9,D9,G9] ]),
	carre([ [B3,E3,H3],[B6,E6,H6],[B9,E9,H9] ]),
	carre([ [C3,F3,I3],[C6,F6,I6],[C9,F9,I9] ]).
