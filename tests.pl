% ce fichier contient juste des bouts de code pour tester le programme
% faire des copié collé dans l'interpréteur prologue

consult('exercice 3').
consult('exercice 4').

% sudoku valide pour tester: (doit répondre true)
% (changer juste un chiffre et voir si ça répond bien false)
lignes_correctes([ [ [[5,3,4],[6,7,2],[1,9,8]],
				[[6,7,8],[1,9,5],[3,4,2]],
				[[9,1,2],[3,4,8],[5,6,7]] ],
				[ [[8,5,9],[4,2,6],[7,1,3]],
				[[7,6,1],[8,5,3],[9,2,4]],
				[[4,2,3],[7,9,1],[8,5,6]] ],
				[ [[9,6,1],[2,8,7],[3,4,5]],
				[[5,3,7],[4,1,9],[2,8,6]],
				[[2,8,4],[6,3,5],[1,7,9]] ] ]).

% sudoku valide pour tester: (doit répondre true)
% (changer juste un chiffre et voir si ça répond bien false)
colonnes_correctes([ [ [[5,3,4],[6,7,2],[1,9,8]],
				[[6,7,8],[1,9,5],[3,4,2]],
				[[9,1,2],[3,4,8],[5,6,7]] ],
				[ [[8,5,9],[4,2,6],[7,1,3]],
				[[7,6,1],[8,5,3],[9,2,4]],
				[[4,2,3],[7,9,1],[8,5,6]] ],
				[ [[9,6,1],[2,8,7],[3,4,5]],
				[[5,3,7],[4,1,9],[2,8,6]],
				[[2,8,4],[6,3,5],[1,7,9]] ] ]).

% pour tester juste une série de 9 chiffres
carre([[1,2,3],[4,5,6],[7,8,9]]).

% la grille exo 5 question 2:
[ [ [[2,_,_],[_,6,_],[_,1,7]],
[[_,7,9],[_,_,_],[_,5,4]],
[[_,8,_],[_,_,_],[6,9,_]] ],
[ [[_,_,_],[1,_,_],[9,_,_]],
[[_,_,_],[8,_,_],[_,_,2]],
[[_,_,_],[4,_,_],[_,3,5]] ],
[ [[_,7,_],[_,_,5],[_,_,_]],
[[_,_,_],[_,6,_],[_,2,_]],
[[_,_,_],[8,_,_],[_,_,_]] ] ]