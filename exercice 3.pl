valeur(X) :-
	select(X, [1,2,3,4,5,6,7,8,9], _).


different(_,[]).

different(E,[H|T]) :-  % on vérifie qu'un élément est différent de la suite de la liste
	E #\= H,
	different(E,T).

tous_differents([]).   % si la liste est vide, c'est vrai qu'ils sont différents

tous_differents([H|T]) :-  % pour chaque élément de la liste, on vérifie qu'il est différents avec le reste
	different(H,T),
	tous_differents(T).


carre1([[A,B,C],[D,E,F],[G,H,I]]) :-
	tous_differents([A,B,C,D,E,F,G,H,I]),
	valeur(A),valeur(B),valeur(C),
	valeur(D),valeur(E),valeur(F),
	valeur(G),valeur(H),valeur(I).

carre2([[A,B,C],[D,E,F],[G,H,I]]) :-
	valeur(A),valeur(B),valeur(C),
	valeur(D),valeur(E),valeur(F),
	valeur(G),valeur(H),valeur(I),
	tous_differents([A,B,C,D,E,F,G,H,I]).

% la fonction carre1 est environ 100 fois plus rapide que la fonction carre2,
% nous allons donc utiliser la fonction carre1

carre([[A,B,C],[D,E,F],[G,H,I]]) :- carre1([[A,B,C],[D,E,F],[G,H,I]]).